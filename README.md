# README #

This is a sample policy that is used to restrict access to resources by tag.  

### What is this repository for? ###

This is meant to restrict every resource that is not explicitly allowed with a Tag named "ACCESS" with a value you specify.  The idea being that application teams can have isolated access to resources within a shared AWS account, reducing the potential impact across application teams.

### How do I get set up? ###

* Create a tag named "ACCESS" with any value (e.g. GMAS, OPER etc...)  
* Because this is a sample, the value we used is "XYZ", please replace with your own
* Because this is a sample, the account number in the policy needs to be changed to your own
* Assign the Tag to the resources you want access to (e.g. ec2-instances)
* Create a test user in IAM and assign this policy and the required forceMFA policy
* Log in as the test user and test restricted access

### Who do I talk to? ###

* Mark Ferry - this process is evolving